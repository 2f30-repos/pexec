/*
 * Copyright (c) 2018 Dimitris Papastamos <sin@2f30.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * execute a program from stdin
 *
 * example: ssh user@host pexec ls -la < /bin/ls
 *
 * This is true cloud computing!
 */

#include <sys/stat.h>
#include <sys/syscall.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "arg.h"

char *argv0;

static ssize_t
xread(int fd, void *buf, size_t nbytes)
{
	unsigned char *bp = buf;
	ssize_t total = 0;

	while (nbytes > 0) {
		ssize_t n;

		n = read(fd, &bp[total], nbytes);
		if (n < 0)
			err(1, "read");
		else if (n == 0)
			return total;
		total += n;
		nbytes -= n;
	}
	return total;
}

static ssize_t
xwrite(int fd, const void *buf, size_t nbytes)
{
	const unsigned char *bp = buf;
	ssize_t total = 0;

	while (nbytes > 0) {
		ssize_t n;

		n = write(fd, &bp[total], nbytes);
		if (n < 0)
			err(1, "write");
		else if (n == 0)
			return total;
		total += n;
		nbytes -= n;
	}
	return total;
}

static int
memfd_create(const char *name, unsigned int flags)
{
	return syscall(__NR_memfd_create, name, flags);
}

void
pexec(int ifd, char *argv[], char *envp[])
{
	char buf[BUFSIZ];
	ssize_t n;
	int fd;

	fd = memfd_create(argv[0], 0);
	if (fd < 0)
		err(1, "memfd_create");

	if (fchmod(fd, 0755) < 0)
		err(1, "fchmod");

	while ((n = xread(ifd, buf, sizeof(buf))) > 0)
		xwrite(fd, buf, n);

	if (fexecve(fd, argv, envp) < 0)
		err(1, "fexecve");
}

static void
usage(void)
{
	fprintf(stderr, "usage: pexec [-i] argv[0] [argv[1]...]\n");
	exit(1);
}

int
main(int argc, char *argv[])
{
	char *envp[] = { NULL };
	int iflag = 0;

	ARGBEGIN {
	case 'i':
		iflag = 1;
		break;
	default:
		usage();
	} ARGEND

	if (argc == 0)
		usage();

	extern char **environ;
	pexec(STDIN_FILENO, argv, iflag ? envp : environ);
	/* unreachable */
}
